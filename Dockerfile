FROM node:latest
WORKDIR /app
COPY app.js .
COPY package.json .
RUN npm install
EXPOSE 8090
CMD ["node", "app.js"]
